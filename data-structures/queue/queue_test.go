package queue

import (
	"fmt"
	"testing"
)

func TestLen(t *testing.T) {
	q := New()
	q.Push(1)
	q.Push(1)
	q.Push(1)
	q.Push(1)
	if q.Len() != 4 {
		t.Error("func return invalid result")
	}
}

func TestShiftPush(t *testing.T) {
	q := New()
	q.Push(1)
	q.Push(1)
	q.Push(1)
	q.Push(1)
	q.Shift()
	q.Shift()
	q.Shift()
	q.Shift()
	if q.Len() != 0 || !q.isEmpty() {
		t.Error("func return invalid result")
	}
}

func TestPeek(t *testing.T) {
	q := New()
	q.Push(1)
	if q.Peek() == nil {
		t.Error("func return invalid result")
	}
}

func TestNew(t *testing.T) {
	q := New()

	if !q.isEmpty() ||
		q.len != 0 ||
		q.Len() != 0 {
		t.Error()
	}

	q.Push(1)
	q.Push(2)
	q.Push(3)

	if q.queue[0] != 1 ||
		q.queue[1] != 2 ||
		q.queue[2] != 3 {
		fmt.Println(q.queue)
		t.Error()
	}

	if q.Len() != 3 {
		t.Error()
	}

	a := q.Shift()

	if a != 1 || q.Len() != 2 {
		t.Error()
	}

	b := q.Peek()

	if b != 2 {
		t.Error()
	}
}
